# Nosql CouchDB + Futon for Ubuntu Mate/Debian on RaspberryPi


---
## Install CouchDB and Futon to a RaspberryPi running Raspbian/Debian/Ubuntu
---


A few set up steps are borrowed directly from the Digital Ocean article and repasted here for clarity. Some of the language has been lifted entirely.

Original article: https://www.digitalocean.com/community/tutorials/how-to-install-couchdb-and-futon-on-ubuntu-14-04


### Update Debian

Begin by updating the system:

```
sudo apt-get update
```

Install a the source repositories management software and JQ, the JSON Query (jq) client:

```
sudo apt-get install software-properties-common jq -y
```

Add the PPA that will help us fetch the latest CouchDB version from the appropriate repository:


```
sudo add-apt-repository ppa:couchdb/stable -y
```

Now that we have added a new PPA, let's update the system so that it has the latest package information:


```
sudo apt-get update
```

### Install CouchDB

Note: If it is necessary to remove a previous couchdb installation, begin here:

```
sudo apt-get remove couchdb couchdb-bin couchdb-common -yf
```
Install CouchDB

```
sudo apt-get install couchdb -y
```

This finishes a basic CouchDB install with Futon as the web server front end for management.

### Test the install

```
curl localhost:5984 | jq .
```

Expected output: 

```
{
  "couchdb": "Welcome",
  "uuid": "787e28a66fae9d551ff2a4e711729009",
  "version": "1.6.0",
  "vendor": {
    "version": "15.10",
    "name": "Ubuntu"
  }
}
```

---

### Secure the CouchDB Installation

When CouchDB is installed, it creates a user and a group named couchdb. In this section we will change the ownership and permission of the CouchDB files to the couchdb user and group.

Changing the ownership controls what the CouchDB process can access, and changing the permissions controls who can access the CouchDB files and directories.

Before changing the ownership and permissions, stop CouchDB:

We'll be using init and service respectively to manage the couch process.


```
sudo service couchdb stop
```

Change the ownership of the `/usr/lib/arm-linux-gnueabihf/couchdb`, `/usr/share/couchdb`, and `/etc/couchdb` directories, and the `/usr/bin/couchdb` executable file, such that their owner is couchdb and they belong to the couchdb group.

```
sudo chown -R couchdb:couchdb /usr/lib/arm-linux-gnueabihf/couchdb /usr/share/couchdb /etc/couchdb /usr/bin/couchdb
```

Now, change the permissions of the `/usr/lib/arm-linux-gnueabihf/couchdb`, `/usr/share/couchdb`, and `/etc/couchdb` directories, and the `/usr/bin/couchdb` executable file, such that the couchdb user and the couchdb group have complete access (to the CouchDB installation) while no other user has access to these files and directories.


```
sudo chmod -R 0770 /usr/lib/arm-linux-gnueabihf/couchdb /usr/share/couchdb /etc/couchdb /usr/bin/couchdb
```

Restart CouchDB

```
sudo service couchdb start
```



